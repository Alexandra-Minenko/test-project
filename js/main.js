const scrollBtn = document.querySelector(".scroll-btn");
const choiceSection = document.querySelector(".choice-section-name");

// scroll btn

function scrollToSection(class_name) {
  document.querySelector(class_name).scrollIntoView({
    behavior: "smooth",
  });
}

scrollBtn.addEventListener("click", () => {
  scrollToSection(".choice-section");
});

// slider

let storageProduct = [
  {
    id: 1,
    src: "./images/mirrror/Eternity Images/Circular Catalog Image 1.jpg",
    name: "Eternity",
    forma: "circular",
    minPrice: 119.99,
    maxPrice: 209.99,
    minLength: 50,
    maxLength: 120,
    minHeight: 50,
    maxHeight: 120,
  },
  {
    id: 2,
    src: "./images/mirrror/Fusion Images/Fusion 2.jpg",
    name: "Fusion",
    forma: "elipce",
    minPrice: 144.99,
    maxPrice: 264.99,
    minLength: 50,
    maxLength: 200,
    minHeight: 50,
    maxHeight: 200,
  },
  {
    id: 3,
    src: "./images/mirrror/Apex Images/Apex 1.jpg",
    name: "Apex",
    forma: "rectangular",
    minPrice: 159.99,
    maxPrice: 259.99,
    minLength: 50,
    maxLength: 180,
    minHeight: 50,
    maxHeight: 180,
  },
  {
    id: 4,
    src: "./images/mirrror/Harmony Images/Double Oval Catalog Image.jpg",
    name: "Harmony",
    forma: "double oval",
    minPrice: 189.99,
    maxPrice: 289.99,
    minLength: 80,
    maxLength: 120,
    minHeight: 80,
    maxHeight: 120,
  },
];

const createArrows = () => {
  const arrowLeft = document.createElement("div");
  arrowLeft.className = "arrowLeft";

  const arrowRight = document.createElement("div");
  arrowRight.className = "arrowRight";

  const container = document.querySelector(".slider");
  container.appendChild(arrowLeft);
  container.appendChild(arrowRight);
};

const buildSliderItem = (id, src, name, forma, minPrice) => {
  const div = document.createElement("div");
  div.className = "slider-item";

  const imgWrap = document.createElement("div");
  imgWrap.className = "item-img-wrap";

  const img = document.createElement("img");
  img.className = "item-img";
  (img.id = id), (img.src = src), (img.alt = `${name} mirror`);
  imgWrap.appendChild(img);

  const pText = document.createElement("p");
  pText.className = "item-name";
  pText.innerText = `Espejo ${forma}`;

  const pPrice = document.createElement("p");
  pPrice.className = "item-price";
  pPrice.innerText = `$ ${minPrice}`;

  div.appendChild(imgWrap);
  div.appendChild(pText);
  div.appendChild(pPrice);

  const container = document.querySelector(".slider-container");
  container.appendChild(div);
};

const renderSliderItems = (storage) => {
  let storageLength = storage.length;
  for (let i = 0; i < storageLength; i++) {
    const itemStorage = storage[i];
    buildSliderItem(
      itemStorage.id,
      itemStorage.src,
      itemStorage.name,
      itemStorage.forma,
      itemStorage.minPrice
    );
  }
  createArrows();
};

renderSliderItems(storageProduct);

const sliderContainer = document.querySelector(".slider-container");
let offset = 0;

document.querySelector(".arrowLeft").addEventListener("click", () => {
  if (document.documentElement.clientWidth <= 768) {
    offset = offset + 288;
    if (offset > 864) {
      offset = 0;
    }
    sliderContainer.style.left = -offset + "px";
  } else if (document.documentElement.clientWidth <= 978) {
    offset = offset + 320;
    if (offset > 640) {
      offset = 0;
    }
    sliderContainer.style.left = -offset + "px";
  } else {
    offset = offset + 350;
    if (offset > 350) {
      offset = 0;
    }
    sliderContainer.style.left = -offset + "px";
  }
});

document.querySelector(".arrowRight").addEventListener("click", () => {
  if (document.documentElement.clientWidth <= 768) {
    offset = offset - 288;
    if (offset < -864) {
      offset = 0;
    }
    sliderContainer.style.left = offset + "px";
  } else if (document.documentElement.clientWidth <= 978) {
    offset = offset - 320;
    if (offset < -700) {
      offset = 0;
    }
    sliderContainer.style.left = offset + "px";
  } else {
    offset = offset - 350;
    sliderContainer.style.left = offset + "px";
    if (offset < 0) {
      offset = 350;
    }
  }
});

//transition to another section

const nextSectionBtn = document.querySelector(".next-section-btn");

nextSectionBtn.addEventListener("click", () => {
  const sliderWrap = document.querySelector(".slider-wrap");
  sliderWrap.style.display = "none";

  const getActiveClass = document.querySelector(".active-name");
  getActiveClass.classList.remove("active-name");

  const setActiveClass = document.querySelector(".section-size");
  setActiveClass.classList.add("active-name");

  const productWrap = document.querySelector(".product-wrap");
  productWrap.style.display = "block";
});

//range slider

const inputHeight = document.getElementById("range-height");
const inputWidth = document.getElementById("range-width");
const outputHeight = document.getElementById("output-height");
const outputWidth = document.getElementById("output-width");
const minValueHeight = document.getElementById("minValue-height");
const maxValueHeight = document.getElementById("maxValue-height");
const minValueWidth = document.getElementById("minValue-width");
const maxValueWidth = document.getElementById("maxValue-width");

inputHeight.min = storageProduct[0].minHeight;
inputHeight.max = storageProduct[0].maxHeight;
inputWidth.min = storageProduct[0].minLength;
inputWidth.max = storageProduct[0].maxLength;

outputHeight.innerText = storageProduct[0].minHeight;
outputWidth.innerText = storageProduct[0].minLength;

minValueHeight.innerText = storageProduct[0].minHeight + " см";
maxValueHeight.innerText = storageProduct[0].maxHeight + " см";
minValueWidth.innerText = storageProduct[0].minLength + " см";
maxValueWidth.innerText = storageProduct[0].maxLength + " см";

const rangeParameters = (parametr, el) => {
  parametr.addEventListener("mouseover", () => {
    el.style.display = "block";
  });

  parametr.addEventListener("mouseout", () => {
    el.style.display = "none";
  });
};

rangeParameters(inputHeight, outputHeight);
rangeParameters(inputWidth, outputWidth);

const outputUpdateHeight = (val) => {
  outputHeight.value = val;
  outputHeight.style.left = val - 49 + "px";
};

const outputUpdateWidth = (val) => {
  outputWidth.value = val;
  outputWidth.style.left = val - 49 + "px";
};
